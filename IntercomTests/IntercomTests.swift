//
//  IntercomTests.swift
//  IntercomTests
//
//  Created by sabaz shereef on 01/06/20.
//  Copyright © 2020 sabaz shereef. All rights reserved.
//

import XCTest
@testable import Intercom

class IntercomTests: XCTestCase {
    
    let testproject = ViewController()
    
    private var tableVC: GuestNameListing!
    
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        tableVC = storyboard.instantiateViewController(withIdentifier: "GuestNames") as? GuestNameListing
        // Trigger view load and viewDidLoad()
        _ = tableVC.view
        
    }
    
    override func tearDown() {
        
        super.tearDown()
    }
    
    
    
    func testHasATableView() {
        XCTAssertNotNil(tableVC.tableView)
    }
    
    
    func testTableViewHasDelegate() {
        XCTAssertNotNil(tableVC.tableView.delegate)
    }
    
    func testTableViewHasDataSource() {
        XCTAssertNotNil(tableVC.tableView.dataSource)
    }
    
    func testTableViewConformsToTableViewDataSourceProtocol() {
        XCTAssertTrue(tableVC.conforms(to: UITableViewDataSource.self))
        XCTAssertTrue(tableVC.responds(to: #selector(tableVC.tableView(_:numberOfRowsInSection:))))
        XCTAssertTrue(tableVC.responds(to: #selector(tableVC.tableView(_:cellForRowAt:))))
    }
    
    func testTableViewCellHasReuseIdentifier() {
        let cell = tableVC.tableView(tableVC.tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as? GuestNamesCellTableViewCell
        let actualReuseIdentifer = cell?.reuseIdentifier
        let expectedReuseIdentifier = "GuestNamesCell"
        XCTAssertEqual(actualReuseIdentifer, expectedReuseIdentifier)
    }
    
    func testDistancecalculation() {
        let distance = tableVC.calculatedistance(lat1: tableVC.dublinLatitude, lon1: tableVC.dublinLongitude, lat2: 52.986375, lon2: -6.043701)
        
        XCTAssertEqual(distance, 41.76671597120392)
        
    }
        
}


