//
//  GuestNamesCellTableViewCell.swift
//  Intercom
//
//  Created by sabaz shereef on 05/06/20.
//  Copyright © 2020 sabaz shereef. All rights reserved.
//

import UIKit

class GuestNamesCellTableViewCell: UITableViewCell {
    @IBOutlet weak var resultLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
