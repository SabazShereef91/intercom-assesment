//
//  Extensions.swift
//  Intercom
//
//  Created by sabaz shereef on 03/06/20.
//  Copyright © 2020 sabaz shereef. All rights reserved.
//

import Foundation
import UIKit


extension BinaryInteger {
    var degreesToRadians: CGFloat { CGFloat(self) * .pi / 180 }
}

extension FloatingPoint {
    var degreesToRadians: Self { self * .pi / 180 }
    var radiansToDegrees: Self { self * 180 / .pi }
}


extension String {
    
    var parseJSONString: AnyObject? {
        
        let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        if let jsonData = data {
            do{
                return try? JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
            }
        } else {
            return nil
        }
    }
}


extension Double {
    func deg2rad() -> Double {
        return self * Double.pi / 180
    }
    
    func rad2deg() -> Double {
        return self * 180.0 / Double.pi
    }
}

