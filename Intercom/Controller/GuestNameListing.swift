//
//  GuestNameListing.swift
//  Intercom
//
//  Created by sabaz shereef on 06/06/20.
//  Copyright © 2020 sabaz shereef. All rights reserved.
//

import UIKit

class GuestNameListing: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    var dublinLatitude =  53.339428
    var dublinLongitude = -6.257664
    var invitedFriendsArray:[[String: Any]] = []
    @IBOutlet weak var tableView: UITableView!
  
    override func viewDidLoad() {
        
        guard let fileReadPath = Bundle.main.path(forResource: "customers", ofType: "txt") else {return}
        
        do {
            let dataString = try String(contentsOfFile: String(describing: fileReadPath), encoding: .utf8)
            var separatedDataString:[String] = dataString.components(separatedBy: .newlines)
            separatedDataString.removeAll(where: {$0 == ""})
            
            for line in separatedDataString {
                guard let json: [String : Any] = line.parseJSONString as? [String : Any],
                    let latitudestr = json["latitude"] as? NSString,
                    let longitudestr = json["longitude"] as? NSString else {
                        continue
                }
                let latitudeDouble = latitudestr.doubleValue
                let longitudeDouble = longitudestr.doubleValue
                let distanceToOffice = calculatedistance(lat1: dublinLatitude, lon1: dublinLongitude, lat2: latitudeDouble, lon2: longitudeDouble)
                
                if distanceToOffice < 100.00 {
                    invitedFriendsArray.append(json)
                }
            }
            invitedFriendsArray = invitedFriendsArray.sorted { ($0["user_id"] as! Int) < ($1["user_id"] as! Int)}
            writeDataTofile(arrayToWrite: invitedFriendsArray)
            }
        catch{ print(error) }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invitedFriendsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GuestNamesCell", for: indexPath) as? GuestNamesCellTableViewCell else { fatalError("Misconfigured cell type!") }
        guard  let guestID = invitedFriendsArray[indexPath.row]["user_id"] as? Int else {fatalError()}
         guard  let guestName = invitedFriendsArray[indexPath.row]["name"] as? String else {fatalError()}
        cell.resultLabel.text = " Guest ID: \(String(describing: guestID) )    Guest Name: \(String(describing: guestName))"
        return cell
        
    }
    
    
    //MARK:- Write file

    func writeDataTofile(arrayToWrite: [[String : Any]]) {
        
        guard let fileWritePath = Bundle.main.path(forResource: "output", ofType: "txt") else {return}
        print(fileWritePath)
        var output: String = ""
        
        for j in 0..<arrayToWrite.count {
            
            let dict = arrayToWrite[j] as AnyObject
            guard let guestId = dict["user_id"] as? Int,let guestName = dict["name"] as? String else {continue}
            let outputStr = "ID: \(guestId)    Guest Name: \(guestName)\n"
            output.append(outputStr)
        }
        do {
            let fileUrl = URL(fileURLWithPath: fileWritePath)
            let fileHandle = try FileHandle(forWritingTo: fileUrl)
            let textData = Data(output.utf8)
            fileHandle.write(textData)
            fileHandle.closeFile()
        } catch {
            print(error)
        }
    }
    //MARK:- Get distance

func calculatedistance(lat1:Double, lon1:Double, lat2:Double, lon2:Double) -> Double {
    let theta = lon1 - lon2
    var distance = sin(lat1.deg2rad()) * sin(lat2.deg2rad()) + cos(lat1.deg2rad()) * cos(lat2.deg2rad()) * cos(theta.deg2rad())
    distance = acos(distance)
  distance = distance.rad2deg()
    distance = distance * 60 * 1.1515
    distance = distance * 1.609344
    return distance
}
}
