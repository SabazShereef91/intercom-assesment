
				INTERCOM TAKE HOME TEST

Take home assignment from intercom was to read data from the text file and to invite guest to the office who are within 100 km from office.

I have shared a bitbucket link to clone my project.

1. After cloning of the project, Go to intercom folder and open intercom.xcodeproj

2. The source code is in here, After clicking on the run button the program runs.

3. The first page has a button with title "Click here to Invite Friends".

4. On clicking this  button the program moves to next view controller where the code for calculating distance, sorting of array and so on are written, and it also shows the result of guest in a table view.

5. I have also written the code to write the result to a text file output.txt, the file is saved bundle applications of the device, for example -->

"/Users/sabazshereef/Library/Developer/CoreSimulator/Devices/3439406D-EC6C-4AEC-B95C-376075676628/data/Containers/Bundle/Application/122073A1-2110-4DE3-A7FD-D796F640281E/Intercom.app/output.txt"

6. For an easy viewing I have added text file "output.txt" at the project directory along with the result in it.

7. I have also written the test cases code in the "intercomTests"